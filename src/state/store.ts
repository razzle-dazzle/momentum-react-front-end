import { createStore as reduxCreateStore, applyMiddleware, compose  } from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage';
import storageSession from 'redux-persist/lib/storage/session';
import thunk from 'redux-thunk';

import { ApplicationState } from './types';

const persistConfig = {
  key: 'root',
  storage: storageSession,
}

const middleware = [thunk];

const initialState: ApplicationState = { count: 1 };
const reducer = (state: ApplicationState = initialState, action: any): ApplicationState => {
  if (action.type === `INCREMENT`) {
    return { ...state, count: state.count + 1 };
  }
  return state;
};

const persistedReducer = persistReducer(persistConfig, reducer);


const createStore = () => {
  const store = reduxCreateStore(persistedReducer, undefined, applyMiddleware(...middleware));
  const persistor = persistStore(store);

  return { store, persistor }
};

export default createStore;
