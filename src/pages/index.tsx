import * as React from 'react';
import { connect } from 'react-redux';
import Layout from '../components/layout';
import Source from '../components/source';
import { ApplicationState } from '../state/types';

interface Props {
  count: number;
  onButtonClick(): void;
}

const IndexPage = ({ count, onButtonClick }: Props): any => {
  return (
    <Layout>
      <h1>{`Hola`} TypeScript world!</h1>
      <p>
        This site is named <strong>{`#${count}`}</strong>
      </p>

      <button type="button" onClick={onButtonClick}>
        Press me
      </button>

      <Source description="Interested in details of this site?" />
    </Layout>
  );
};

const mapStateToProps = ({ count }: ApplicationState) => ({ count });
const mapDispatchToProps = (dispatch: any) => ({
  onButtonClick: () => dispatch({ type: `INCREMENT` }),
});

export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
